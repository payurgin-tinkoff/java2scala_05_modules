package ru.tinkoff.simpleapp.common

import slick.dbio.{DBIOAction, Effect, NoStream}

package object db {
  type IO[+R, -E <: Effect] = DBIOAction[R, NoStream, E]
}

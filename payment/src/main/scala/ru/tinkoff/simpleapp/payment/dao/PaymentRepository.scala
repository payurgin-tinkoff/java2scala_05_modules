package ru.tinkoff.simpleapp.payment.dao

import java.util.UUID

import ru.tinkoff.simpleapp.common.db.DB
import ru.tinkoff.simpleapp.payment.service.Payment

import scala.concurrent.Future

class PaymentRepository(db: DB, paymentsTable: PaymentsTable) {

  import paymentsTable._
  import db.profile.api._

  def create(payment: Payment): Future[Int] = db.run(
    AllPayments += payment
  )

  def list(): Future[Seq[Payment]] = db.run(AllPayments.result)

  def findPayment(id: UUID): Future[Option[Payment]] = {
    db.run(
      AllPayments.filter(_.id === id.bind).result.headOption
    )
  }

}

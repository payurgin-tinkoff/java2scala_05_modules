package ru.tinkoff.simpleapp.payment

import com.softwaremill.macwire._
import ru.tinkoff.simpleapp.common.RuntimeModule
import ru.tinkoff.simpleapp.common.api.{ApiModule, Controller}
import ru.tinkoff.simpleapp.common.db.DatabaseModule
import ru.tinkoff.simpleapp.notification.NotificationModule
import ru.tinkoff.simpleapp.payment.controller.PaymentController
import ru.tinkoff.simpleapp.payment.dao.{PaymentRepository, PaymentsTable}
import ru.tinkoff.simpleapp.payment.service.{PaymentService, PaymentServiceImpl}

import scala.annotation.unused

@Module
class PaymentModule(runtimeModule: RuntimeModule,
                    @unused notificationModule: NotificationModule,
                    @unused databaseModule: DatabaseModule) extends ApiModule {
  import runtimeModule._

  val paymentDB: PaymentsTable = wire[PaymentsTable]
  @unused private val paymentRepository: PaymentRepository = wire[PaymentRepository]
  @unused private val paymentService: PaymentService = wire[PaymentServiceImpl]

  val paymentController: PaymentController = wire[PaymentController]

  override val controllers: Seq[Controller] = wireSet[Controller].toSeq
}
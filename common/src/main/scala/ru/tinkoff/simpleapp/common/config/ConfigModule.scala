package ru.tinkoff.simpleapp.common.config

import com.typesafe.config.{Config, ConfigFactory}

class ConfigModule {
  val config: Config = ConfigFactory.load()
}
